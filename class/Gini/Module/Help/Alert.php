<?php

namespace Gini\Module\Help;

class Alert
{
    // 未绑定
    const TYPE_OK = 'TYPE_OK';
    // 已绑定
    const TYPE_ERROR = 'TYPE_ERROR';

    public static function setMessage($message, $type) {
        switch ($type) {
            case 'success':
                $type = self::TYPE_OK;
                break;
            case 'danger':
                $type = self::TYPE_ERROR;
                break;
        }

        if (!in_array($type, [self::TYPE_OK, self::TYPE_ERROR])) {
            $type = self::TYPE_OK;
        }
        $messages = (array)$_SESSION['Alert.Message'];

        $_PUSHMESSAGE = function($type, $message) use($messages) {
            !is_array($messages[$type]) and $messages[$type] = [];
            $messages[$type][] = $message;
            $_SESSION['Alert.Message'] = $messages;
        };

        $type == self::TYPE_OK
            and !$messages[self::TYPE_ERROR]
            and $_PUSHMESSAGE($type, $message);

        $type == self::TYPE_ERROR
            and !$messages[self::TYPE_OK]
            and $_PUSHMESSAGE($type, $message);
    }

    public static function getMessage() {
        if ($_SESSION['Alert.Message']) {
            $messages = $_SESSION['Alert.Message'];
            $_SESSION['Alert.Message'] = [];
            $key = $messages[self::TYPE_ERROR] ? self::TYPE_ERROR : self::TYPE_OK;
            $message = join('<p/>', $messages[$key]);
            return V('help/message', [
                    'type' => $key,
                    'value' => $message,
                ]);
        }
        else {
            return '';
        }
    }
}