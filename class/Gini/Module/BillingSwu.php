<?php

namespace Gini\Module {

    class BillingSwu {

        static function setup() {
            
            date_default_timezone_set(\Gini\Config::get('system.timezone') ?: 'Asia/Shanghai');

            class_exists('\Gini\Those');

            self::setOAuth();

            // OAuth based SSO support
            if (PHP_SAPI != 'cli') {
                if ($_GET['oauth-sso']) {
                    $host = $_SERVER['HTTP_HOST'];
                    $oauth = \Gini\IoC::construct('\Gini\OAuth\Client', $_GET['oauth-sso']);
                    $username = $oauth->getUserName();
                    if ($username) {
                        \Gini\Auth::login($username);
                    }
                }

                $username = \Gini\Auth::username();

                $me = a('user', $username ? ['username'=>$username] : null);

                if ($username && (!$me->id || time() - $me->lastSyncTime > 86400 || !$me->lastSyncTime || true)) {
                    $me->username = $username;
                    $me->update();
                }
                
                _G('ME', $me);
            }
            
            setlocale(LC_MONETARY, \Gini\Config::get('system.locale') ?: 'zh_CN');
            \Gini\I18N::setup();
        }

        static function setOAuth($host = null) {
            $key = 'billing.' . strtolower(getenv('GINI_ENV'));
            $host = $host ? : $_SERVER['HTTP_HOST'];

            if ($host) {
                $host = 'http://' . $host . '/lims/';
                $client = \Gini\Config::get('oauth.client');
                $client['servers'][$key]['auth'] = $host . '!oauth/provider2/index';
                $client['servers'][$key]['token'] = $host . '!oauth/provider2/access_token';
                $client['servers'][$key]['user'] = $host . '!oauth/provider2/user';
                \Gini\Config::set('oauth.client', $client);

                $billing = \Gini\Config::get('server.remote');
                $billing[$key]['api'] = $host . 'api';
                \Gini\Config::set('server.remote', $billing);

                $backends = \Gini\Config::get('auth.backends');
                $backends['database']['rpc.url'] = $host . 'api';
                \Gini\Config::set('auth.backends', $backends);
            }
        }

        static function serverTitle($prefer_short=false) {
            $server = self::localServer();
            if ($prefer_short) return $server['short_title'] ? : $server['site'] . $server['title'];
            return [$server['site'] , $server['title']];
        }

        static function remoteServer($name = null) {
            if (!$name) return \Gini\Config::get('server.remote');
            $servers = \Gini\Config::get('server.remote');
            return isset($servers[$name]) ? $servers[$name] : null;
        }

        static function localServer() {
            return \Gini\Config::get('server.local');
        }

    }
}

namespace {

    if (function_exists('m')) {
        die('m() was declared by other libraries, which may cause problems!');
    } else {
        function m($name, $params = null)
        {
            $class_name = '\Gini\Model\\'.str_replace('/', '\\', $name);

            return \Gini\IoC::construct($class_name, $params);
        }
    }

    if (function_exists('alert')) {
        die('alert() was declared by other libraries, which may cause problems!');
    } else {
        function alert($text, $type = \Gini\Module\Help\Alert::TYPE_OK)
        {
            \Gini\Module\Help\Alert::setMessage($text, $type);
        }
    }

    if (function_exists('thoseIndexed')) {
        die('thoseIndexed() was declared by other libraries, which may cause problems!');
    } else {
        function thoseIndexed($name)
        {
            return \Gini\IoC::construct('\Gini\Module\ThoseIndexed', $name);
        }
    }
    
}
