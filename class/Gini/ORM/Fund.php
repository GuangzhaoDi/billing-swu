<?php

namespace Gini\ORM;

class Fund extends Billing\Remote
{
    public $dept_name   = 'string:100';
    public $dept_no     = 'string:100';
    public $prot_name   = 'string:50';
    public $prot_no     = 'string:100';
    public $ctime       = 'datetime';

    protected $cache = true;
    protected $cacheTimeout = 86400;

    public function makeName () {
        return "[{$this->prot_no}] {$this->prot_name}";
    }

    public function balance () {
        $me = _G('ME');
        try {
		    $rest = $this->getREST();
            return $rest->get('fund', [
                'dept' => $this->dept_no,
                'prot' => $this->prot_no,
                'ref' => $me->ref,
            ]);
		} catch (\Gini\REST\Exception $e) {
			return false;
		}
    }

    public function unfreeze ($args) {
        $filter_key = ['fund_id', 'lab_id', 'account_id'];
        $filter = ['action' => 'unfreeze'];
        if (is_numeric($args['fund_id']) && is_numeric($args['lab_id'])) {
            $filter['fund_id'] = $args['fund_id'];
            $filter['lab_id'] = $args['lab_id'];
            $filter['money'] = $args['money'];
            $filter['user'] = _G('ME');
        } elseif (is_numeric($args['account_id']) && is_numeric($args['money'])) {
            $filter['account_id'] = $args['account_id'];
            $filter['money'] = $args['money'];
            $filter['user'] = _G('ME');
        } else {
            return false;
        }
        $me = _G('ME');
        try {
            $rest = $this->getREST();
            return $rest->put('fund', $filter);
        } catch (\Gini\REST\Exception $e) {
            return false;
        }
    }
    public function account () {
        $me = _G('ME');
        try {
		    $rest = $this->getREST();
            $accounts = $rest->get('account', [
                'lab' => $me->lab->oid,
                'fund' => $this->id,
            ]);
            unset($accounts['total']);
            return [
                'total' => array_sum(array_column($accounts, 'total')),
                'balance' => array_sum(array_column($accounts, 'balance'))
            ];
		} catch (\Gini\REST\Exception $e) {
			return false;
		}
    }

    public function links ($lab_id = null) {
        $links = [];

        $links['info'] = [
            'title' => T('查看'),
            // 'class' => 'fa fa-search',
            'url' => "deposit/fund/{$this->id}",
        ];
        if (_G('ME')->isPi()) {
            $links['batch_unfreeze'] = [
                'title' => T('批量解冻'),
                // 'class' => 'fa fa-search',
                'url' => "gini-ajax:ajax/Deposit/batchUnfreeze/{$this->id}/{$lab_id}",
            ];
        }
        return \Gini\Model\Widget::factory('links', ['items' => $links]);
    }
}