<?php

namespace Gini\ORM;

class User extends Billing\Object
{
    public $name            = 'string:50';
    public $name_abbr       = 'string:100';
    public $username        = 'string:120';
    public $isadmin         = 'bool';
    public $isowner         = 'bool';
    public $ischarger       = 'bool';
    public $card            = 'string:50';
    public $ref             = 'string:10';
    public $email           = 'string:120';
    public $phone           = 'string:120';
    public $group           = 'array';
    public $lab             = 'object:lab';
    public $ctime           = 'datetime';
    public $lastSyncTime    = 'int';
    public $oid             = 'int';
    public $oname           = 'string:50';

    protected static $db_index = [
        'unique:username',
        'unique:oid',
        'name_abbr', 'ctime',
        'ref', 'lab'
    ];

    public static $_RPCs;

    private function getRPC() {
        class_exists('\Gini\RPC');
        $username = $this->username;
        list($username, $key) = \Gini\Auth::parseUserName($username);
        
        if ($key) {
            if (!preg_match('/(.+)%([^%]+)$/', $key, $parts)) {
                throw new \Gini\RPC\Exception("未知服务器: $key");
            }
            $backend = \Gini\Auth::backends()[$key];
            $id = $backend['info_server'] ? : $parts[2];
            if (!$id) {
                throw new \Gini\RPC\Exception("无法验证服务器");
            }
        }
        else {
            $id = array_keys(\Gini\Config::get('server.remote'))[0];
        }
        
        if (!isset(self::$_RPCs[$id])) {
            $server = \Gini\Module\BillingSwu::remoteServer($id);
            $client = \Gini\Config::get('rpc.clients')['people'];
            $rpc = \Gini\IoC::construct('\Gini\RPC', $server['api']);
            $sign = $rpc->people->authorize($client['client_id'], $client['client_secret']);
            if (!$sign) {
                throw new \Gini\RPC\Exception("无法验证服务器: $id");
            }

            self::$_RPCs[$id] = $rpc;
        }

        return self::$_RPCs[$id];
    }

    static function user_ACL($e, $user, $action, $object, $when, $where) {
        switch ($action) {
            case '授权':
                if ($user->isPi() || $user->isAdmin()) {
                    return TRUE;
                }
                return FALSE;
                break;
            case '充值':
                if ($user->isPi() || $user->isAdmin()) {
                    return TRUE;
                }
                return FALSE;
                break;
        }
        return $e->pass();
    }

    public function save() {
        if ($this->ctime == '0000-00-00 00:00:00' || !isset($this->ctime)) $this->ctime = date('Y-m-d H:i:s');
        return parent::save();
    }

    public function isAdmin() {
        return $this->isadmin;
    }

    public function isPi() {
        return $this->isowner;
    }

    public function isCharger() {
        return $this->ischarger;
    }

    public function update() {
        $username = $this->username;
        list($username, $key) = \Gini\Auth::parseUserName($username);

        if ($key && !preg_match('/(.+)%([^%]+)$/', $key, $parts)) {
            return;
        }
        
        $username = $key ? \Gini\Auth::makeUserName($username, $parts[1]) : $this->oid;

        $rpc = $this->getRPC();
        $info = $rpc->people->get_user($username);
        
        $this->email = $info['email'];
        $this->name = $info['name'];
        $this->card = $info['card_no'];
        $this->ref = $info['ref_no'];
        $this->ctime = $info['ctime'];
        $this->phone = $info['phone'];
        $this->group = $info['group'];
        $this->roles = $info['roles'];
        $this->isowner = $info['lab_owner'] ? 1 : 0;
        $this->isadmin = $info['is_admin'] ? 1 : 0;
        $this->ischarger = $info['is_incharge'] ? 1 : 0;
        $this->lastSyncTime = time();
        $this->oid = (int)$info['id'];
        $this->oname = $info['source'];

        if ($info['lab_id']) {
            $lab = a('lab', ['oid' => $info['lab_id']]);
            if (!$lab->id || time() - $lab->lastSyncTime > 86400 || !$lab->lastSyncTime) {
                $lab->oid = $info['lab_id'];
                $lab->updateInfo();
            }
            $this->lab = $lab;
        }

        $this->save();

        if ($this->isowner && !$this->lab->owner->id) {
            $this->lab->owner = $this;
            $this->lab->save();
        }

        if ($info['is_incharge']) {
            foreach ($info['equipment'] as $equipment) {
                $connect = a('user/equipment', [
                    'user' => $this,
                    'equipment' => $equipment,
                ]);
                $connect->user = $this;
                $connect->equipment = $equipment;
                $connect->save();
            }
        }
    }
    /*
        dgz 修改栏目顺序
    */
    public function sidebarItems() {
        $items = [];
        if ($this->isCharger() && !$this->isAdmin()) {
            $items['distribution'] = [
                'icon' => 'fa fa-address-card-o',
                'text' => '分配结果'
            ];
        }
      
        $items['authorized/my'] = [
            'icon' => 'fa fa-address-card-o',
            'text' => '我的授权'
        ];

        $items['transaction/my'] = [
            'icon' => 'fa fa-newspaper-o',
            'text' => '我的明细'
        ];
        
        //经费管理，经费充值 合并为经费列表
        if ($this->isAllowedTo('充值')) {
            $items['fund'] = [
                'icon' => 'fa fa-cubes',
                'text' => '经费列表',
            ];
             $items['deposit'] = [
                'icon' => 'fa fa-jpy',
                'text' => '充值冻结'
            ];
        }

        //经费授权”与“授权信息”合并为授权管理
        if ($this->isAllowedTo('授权')) {
            $items['authorized'] = [
                'icon' => 'fa fa-handshake-o',
                'text' => '授权管理'
            ];
        }

        if ($this->isAllowedTo('充值')) {
            $items['transaction'] = [
                'icon' => 'fa fa-list',
                'text' => '财务明细'
            ];
        }

        if ($this->isAllowedTo('审核')) {
            $items['distribution'] = [
                'icon' => 'fa fa-check-square-o',
                'text' => '分配审批'
            ];
        } 

        if ($this->isAdmin()) {
            if ($this->isAllowedTo('充值')) {
                $items['transaction/system'] = [
                    'icon' => 'fa fa-list',
                    'text' => '系统财务明细'
                ];
            }

            $items['limit'] = [
                'icon' => 'fa fa-lock',
                'text' => '充值限制'
            ];
        }
       
        return $items;
    }

    public function defaultTab() {
        $tab = 'authorized/my';

        if ($this->isAllowedTo('授权')) {
            $tab = 'authorized';
        }
        else if ($this->isAllowedTo('充值')) {
            $tab = 'deposit';
        }
        else if ($this->isAllowedTo('审核')) {
            $tab = 'distribution';
        }
        return $tab;
    }

    public function defaultUrl() {
        return $this->defaultTab();
    }

}
