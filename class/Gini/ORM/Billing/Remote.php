<?php

namespace Gini\ORM\Billing;

class Remote extends Object
{    
    protected $cache = false;
    protected $cacheTimeout = 60;

    protected static $_REST = [];
    protected function getREST($type = 'billing')
    {
        if (!isset(self::$_REST[$type])) {
            $conf = \Gini\Config::get('app.rest')[$type];
            try {
                $rest = \Gini\IoC::construct('\Gini\REST', $conf['url'], $conf['path']);
                self::$_REST[$type] = $rest;
            } catch (RPC\Exception $e) {
            }
        }

        return self::$_REST[$type];
    }
    
    public function convertRPCData(array $rdata) {
        $data = [];
        $data['id'] = $rdata['id'];
        $child = get_class_vars(get_class($this));
        $parent = get_class_vars(__CLASS__);

        foreach (array_diff($child, $parent) as $key => $type) {
            $data[$key] = $rdata[$key];
        }

        $data['_extra'] = J(
            array_diff_key($rdata, array_flip(array_keys($data)))
        );

        return $data;
    }

    public function fetch($force = false)
    {
        if ($force || $this->_db_time == 0) {
            if (is_array($this->_criteria) && count($this->_criteria) > 0) {
                $criteria = $this->normalizeCriteria($this->_criteria);
                $id = isset($criteria['id']) ? $criteria['id'] : $criteria['uuid'];
                $key = $this->name().'#'.$id;
                $cacher = \Gini\Cache::of('remote');
                $data = $id ? $cacher->get($key) : null;
                if (is_array($data)) {
                    \Gini\Logger::of('orm')->debug("cache hits on $key");
                } else {
                    \Gini\Logger::of('orm')->debug("cache missed on $key");
                    $rdata = $id ? $this->fetchRPC($id) : $this->fetchRPC($criteria);
                    if (is_array($rdata) && count($rdata) > 0) {
                        $data = $this->convertRPCData($rdata);
                        // set ttl to cacheTimeout sec
                        if ($this->cache) $cacher->set($key, $data, $this->cacheTimeout);
                    }
                }
            }

            $this->setData((array) $data);
        }
    }

    public function fetchRPC($data)
    {
        try {
            $name = $this->name();
            $rest = self::getRest();
            if (is_array($data)) {
                $result = $rest->get($name, $data);
            }
            else {
                $result = $rest->get("{$name}/{$data}");
            }
            return $result;
        } catch (\Gini\RPC\Exception $e) {
            return [];
        }
    }

    public function db()
    {
        return false;
    }

    public function save($params = []) {
        if ($this->ctime == '0000-00-00 00:00:00' || !isset($this->ctime)) $this->ctime = date('Y-m-d H:i:s');
        try {
            $name = $this->name();
            $rest = self::getRest();
            if (!$this->id) {
                $result = $rest->post($name, $this);
            }
            else {
                $result = $rest->put("{$name}/{$this->id}", $this);
            }
            $data = $this->convertRPCData($result);
            return $this->setData($data);
        }
        catch (\Gini\REST\Exception $e) {
            return false;
        }
    }

    public function update($data = []) {
        $name = $this->name();
        $rest = $this->getRest();
        try {
            $result = $rest->patch("{$name}/{$this->id}", $data);
            $data = $this->convertRPCData($result);
            return $this->setData($data);
        }
        catch (\Gini\REST\Exception $e) {
            return false;
        }
    }

    public function delete()
    {
        return false;
    }

}
