<?php

namespace Gini\ORM\User;

class Equipment extends \Gini\ORM\Billing\Object
{
    public $user        = 'object:user';
    public $equipment   = 'string:50';
    public $ctime       = 'datetime';

    protected static $db_index = [
        'user', 'equipment'
    ];

    public function save () {
        if ($this->ctime == '0000-00-00 00:00:00' || !isset($this->ctime)) $this->ctime = date('Y-m-d H:i:s');
        return parent::save();
    }
}