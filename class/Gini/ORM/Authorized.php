<?php

namespace Gini\ORM;

class Authorized extends Billing\Remote
{
    public $account     = 'int';
    public $balance     = 'int';
    public $type        = 'int';
    public $ctime       = 'datetime';

    public function connect ($user) {
        $me = _G('ME');
        try {
            $rest = self::getRest();
            $result = $rest->post('authorized/user', [
                'me' => $me->oid,
                'user' => $user,
                'authorized' => $this->id,
            ]);
            return $result;
        }
        catch (\Gini\REST\Exception $e) {
            return false;
        }
    }

    public function disconnect ($user) {
        $me = _G('ME');
        try {
            $rest = self::getRest();
            $connect = current(thoseIndexed('authorized\user')->filter([
                'authorized' => $this->id,
                'user' => $user,
            ])->fetch());
            $id = $connect['id'];
            return $rest->delete("authorized/user/{$id}", [
                'me' => $me->oid,
            ]);
        }
        catch (\Gini\REST\Exception $e) {
            return false;
        }
    }

    public function links () {
        $links = [];

        $links['check'] = [
            'title' => T('查看'),
            // 'class' => 'fa fa-search',
            'url' => "transaction/authorized/{$this->id}",
        ];

        return \Gini\Model\Widget::factory('links', ['items' => $links]);
    }

}
