<?php

namespace Gini\ORM;

class Equipment extends Billing\Object
{
    public $oid             = 'int,default:0';
    public $name            = 'string:100';//名称
    public $addr            = 'string:150';//拼音
    public $ref_no          = 'string:50';//编号
    public $cat_no          = 'string:50';//分类号
    public $model_no        = 'string:50';//型号
    public $price           = 'double,default:0';//价格
    public $group           = 'array';//组织机构
    public $ctime           = 'int:11';//创建时间
    public $status          = 'int,default:0';//状态
    public $manufacturer    = 'string:100';//生产厂家
    public $purchased_date  = 'int:11';//购置日期
    public $atime           = 'int:11';
    public $charger         = 'string:100';//负责人
    public $contact         = 'string:100';//联系人
    public $duration        = 'double,default:0';

    protected static $db_index = [
        'oid', 'name', 'addr',
        'status'
    ];

    public function save () {
        if ($this->ctime == '0000-00-00 00:00:00' || !isset($this->ctime)) $this->ctime = date('Y-m-d H:i:s');
        return parent::save();
    }

    public function group () {
        $group = $this->group;
        $config = \Gini\Config::get('billing.distribution');
        foreach ($config['manager']['account'] as $id => $account) {
            if (array_key_exists($id, $group)) {
                return $group[$id];
            }
        }
        return '';
    }

}
