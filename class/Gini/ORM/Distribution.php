<?php

namespace Gini\ORM;

class Distribution extends Billing\Remote
{
    public $admin           = 'string:100'; //最高权限分配账号(设备处)
    public $admin_scale     = 'double';
    public $manager         = 'string:100'; //次级权限分配账号(院级)
    public $manager_scale   = 'double';
    public $school          = 'string:100'; //学校分配账号 
    public $school_scale    = 'double';
    public $balance         = 'double';
    public $equipment       = 'int';
    public $status          = 'int,default:1';
    public $reviewe         = 'int';
    public $complete        = 'int';
    public $reserve         = 'string:50';
    public $ctime           = 'datetime';

    const STATUS_PREPARE = 1; //待确认
    const STATUS_COMPLETE = 3; //确认完成
    const STATUS_ASSIGNED = 4; //分配完成

    public static $STATUS_LABEL = [
        self::STATUS_PREPARE => '待确认',
        self::STATUS_COMPLETE => '确认完成',
        self::STATUS_ASSIGNED => '分配完成',
    ];

    static function distribution_ACL ($e, $user, $action, $object, $when, $where) {
        switch ($action) {
            case '审核':
                if ($user->isAdmin()) {
                    return TRUE;
                }
                return FALSE;
                break;
            case '查看':
                if ($user->isAdmin()) {
                    return TRUE;
                }
                else if ($user->isCharger() && in_array($object->status, [
                    self::STATUS_COMPLETE,
                    self::STATUS_ASSIGNED,
                ])) {
                    return TRUE;
                }
                return FALSE;
                break;
        }
        return $e->pass();
    }

    public function label () {
        $vars = [];
        $vars['content'] = self::$STATUS_LABEL[$this->status];
        switch ($this->status) {
            case self::STATUS_PREPARE:
                $vars['class'] = 'label label-info';
                break;
            case self::STATUS_COMPLETE:
                $vars['class'] = 'label label-warning';
                break;
            case self::STATUS_ASSIGNED:
                $vars['class'] = 'label label-success';
                break;
        }
        return \Gini\Model\Widget::factory('label', $vars);
    }

    public function links () {
        $me = _G('ME');
        $links = [];

        if ($me->isAllowedTo('审核') && $this->status == self::STATUS_PREPARE && $this->manager) {
            $links['confirm'] = [
                'title' => T(''),
                'class' => 'fa fa-check-circle-o',
                'url' => "gini-ajax:ajax/distribution/confirm/{$this->id}",
                'extra' => ' data-toggle="tooltip" data-placement="left" title="确认" ',
            ];
        }

        $links['check'] = [
            'title' => T('查看'),
            // 'class' => 'fa fa-search',
            'url' => "distribution/info/{$this->id}",
            'extra' => ' data-toggle="tooltip" data-placement="left" title="查看详情" ',
        ];

        return \Gini\Model\Widget::factory('links', ['items' => $links]);
    }

    public function manager () {
        if (!$this->manager) {
            $equipment = a('equipment', ['oid' => $this->equipment]);
            $group = $equipment->group;
            $config = \Gini\Config::get('billing.distribution');
            foreach ($config['manager']['account'] as $id => $account) {
                if (array_key_exists($id, $group)) {
                    $this->manager = implode('-', 
                    [$account['depart'], $account['project'], $account['subject'], $account['economy']]);
                    if ($this->save()) {
                        break;
                    }
                }
            }
        }
        return $this;
    }

}