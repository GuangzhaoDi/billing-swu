<?php

namespace Gini\ORM\Authorized;

class User extends \Gini\ORM\Billing\Remote
{
    public $user        = 'int';
    public $authorized  = 'int';
    public $ctime       = 'datetime';
}