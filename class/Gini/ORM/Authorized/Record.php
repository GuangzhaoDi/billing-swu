<?php

namespace Gini\ORM\Authorized;

class Record extends \Gini\ORM\Billing\Remote
{
    public $authorized  = 'int';
    public $operator    = 'int';
    public $licensor    = 'int';
    public $balance     = 'int';
    public $type        = 'int';
    public $ctime       = 'datetime';

    protected static $db_index = [
        'operator', 'licensor',
        'type', 'authorized',
    ];
    
    const TYPE_ALLOW = 1; //允许
    const TYPE_REJECT = 2; //拒绝
    const TYPE_PUT = 3; //补充授权
    const TYPE_DEDUCT = 4; //扣除授权

    public static $types_text = [
        self::TYPE_ALLOW => '授权',
        self::TYPE_REJECT => '取消',
        self::TYPE_PUT => '充值',
        self::TYPE_DEDUCT => '扣费',
    ];

    public static $types_style = [
        self::TYPE_ALLOW => 'label label-primary',  //'text-success',
        self::TYPE_REJECT => 'label label-warning', //'text-danger',
        self::TYPE_PUT => 'label label-success',     //'text-success',
        self::TYPE_DEDUCT => 'label label-danger'  //'text-danger',
    ];

}