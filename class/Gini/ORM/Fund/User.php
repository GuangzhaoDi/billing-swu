<?php

namespace Gini\ORM\Fund;

class User extends \Gini\ORM\Billing\Remote
{
    public $user        = 'object:user';
    public $fund        = 'int';
    public $disable     = 'int';
    public $show        = 'int';
    public $ctime       = 'datetime';

    protected static $db_index = [
        'user', 'fund',
        'show', 'disable',
    ];
}