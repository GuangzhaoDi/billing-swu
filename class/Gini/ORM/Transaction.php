<?php

namespace Gini\ORM;

class Transaction extends Billing\Remote
{
    public $account         = 'int';
    public $authorized      = 'int';
    public $distribution    = 'int';
    public $user            = 'int'; 
    public $evidence        = 'string:100'; //凭证号
    public $amount          = 'double,default:0'; //金额
    public $status          = 'int,default:0'; //状态
    public $transfer        = 'int'; //是否为转账
    public $source          = 'string:100'; //费用来源
    public $object          = 'string:100'; //关联对象
    public $subject         = 'string:100'; //不知道叫嘛了 关联对象2
    public $note            = 'string:100'; //该字段为财务明细的补充字段,因可能有多种用途设置为string 西南大学中代表补助费
    public $description     = 'string:500';
    public $comment         = 'string:500';
    public $remark          = 'string:500';
    public $ctime           = 'datetime';

    const STATUS_PREPARE = 0; //待支付
    const STATUS_PAID = 1; //已支付

    public static $STATUS_LABEL = [
        self::STATUS_PREPARE => '待支付',
        self::STATUS_PAID => '已支付',
    ];

    public static $AMOUNT_LABEL = [
        1 => '转入',
        // 2 => '转出',
        3 => '使用',
    ];
    
    public function text () {
        if ($this->transfer) {
            return $this->amount > 0 ? '转入' : '转出';
        }
        else {
            return $this->amount > 0 ? '' : '使用';
        }
    }

    public function style () {
        return $this->amount > 0 ? 'text-success' : 'text-danger';
    }

    public function charge () {
        if ($this->status == \Gini\ORM\Transaction::STATUS_PAID) {
            return str_pad(explode('#', $this->object)[1], 6, 0, STR_PAD_LEFT);
        }
    }
    
}
