<?php

namespace Gini\ORM;

class Account extends Billing\Remote
{
    public $fund        = 'int';
    public $user        = 'int';
    public $lab         = 'int';
    public $balance     = 'int';
    public $total       = 'int';
    public $ctime       = 'datetime';

    public function links () {
        $links = [];

        $links['check'] = [
            'title' => T('查看'),
            // 'class' => 'fa fa-search',
            'url' => "transaction/{$this->id}",
        ];
        if (_G('ME')->isPi()) {
             $links['unfreeze'] = [
                'title' => T('解冻'),
                // 'class' => 'fa fa-search',
                'url' => "gini-ajax:ajax/Deposit/unfreeze/{$this->id}",
            ];
        }
        return \Gini\Model\Widget::factory('links', ['items' => $links]);
    }

    public function serialNo () {
        return 'LS' . $this->id;
    }
}