<?php

namespace Gini\ORM;

class Config extends Object
{
    public $key             = 'string:250';
    public $value           = 'string:500';

    protected static $db_index = [
        'key',
    ];

}