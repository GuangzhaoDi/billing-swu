<?php

namespace Gini\ORM\ThoseIndexed;

abstract class Object
{
    private $_name;
    private $_tableName;
    protected $_total;

    protected static $_REST = [];
    protected static function getREST ($type = 'billing')
    {
        if (!isset(self::$_REST[$type])) {
            $conf = \Gini\Config::get('app.rest')[$type];
            try {
                $rest = \Gini\IoC::construct('\Gini\REST', $conf['url'], $conf['path']);
                self::$_REST[$type] = $rest;
            } catch (RPC\Exception $e) {
            }
        }

        return self::$_REST[$type];
    }

    protected $_criteria = [];
    public function filter (array $criteria)
    {
        $this->_criteria = $criteria;
    }

    public function fetch ($start = 1, $step = 20)
    {
		$rest = $this->getREST();
		if (!$rest) return [];

        if ($start && $step) {
            $this->_criteria['limit'] = [($start - 1) * $step, $step];
        }

		try {
            $name = $this->name();
            $objects = $rest->get($name, $this->_criteria);
            $this->_total = $objects['total'];
            unset($objects['total']);
			return $objects;
		} catch (\Gini\REST\Exception $e) {
			return [];
		}
    }

    public function total () {
        if ($this->_total) {
            return $this->_total;
        }
        else {
            $this->fetch();
            return $this->_total;
        }
    }

    public function db () {
        return false;
    }

    public function name ()
    {
        if (!isset($this->_name)) {
            $this->_prepareName();
        }
        return $this->_name;
    }

    private function _prepareName ()
    {
        // remove Gini/ORM/ThoseIndexed
        list(, , , $name) = explode('/', str_replace('\\', '/', strtolower(get_class($this))), 4);
        $this->_name = $name;
        $this->_tableName = str_replace('/', '_', $name);
    }
}