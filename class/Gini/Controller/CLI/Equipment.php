<?php

namespace Gini\Controller\CLI;

class Equipment extends \Gini\Controller\CLI {

    function actionGet () {
        $remote_server = \Gini\Module\BillingSwu::remoteServer('billing.swu');
        $rpc = \Gini\IoC::construct('\Gini\RPC', $remote_server['api']);

        $result = $rpc->equipment->searchEquipments([]);
        $total = $result['total'];
        $start = 0;
        $step = 1;

        while ($start <= $total) {
            $equipments = $rpc->equipment->getEquipments($result['token'], $start, $step);
            if (count($equipments)) foreach ($equipments as $equipment) {
                $e = a('equipment', ['oid' => $equipment['id']]);
                echo '[' . $equipment['id'] . ']' . $equipment['name'] . "\n";
                $e->oid = $equipment['id'];
                $e->name = $equipment['name'];
                $e->addr = $equipment['name_abbr'];
                $e->ref_no = $equipment['ref_no'];
                $e->cat_no = $equipment['cat_no'];
                $e->model_no = $equipment['model_no'];
                $e->price = (float)$equipment['price'];
                $e->group = $equipment['group'];
                $e->ctime = $equipment['ctime'];
                $e->status = $equipment['status'];
                $e->manufacturer = $equipment['manufacturer'];
                $e->purchased_date = $equipment['purchased_date'];
                $e->atime = $equipment['atime'];
                $e->charger = $equipment['incharges'];
                $e->contact = $equipment['contact'];
                $e->duration = $e->duration ? : 0;
                $e->save();
            }
            $start += $step;
        }

        echo "   \e[32mdone.\e[0m\n";
    }

}
