<?php

namespace Gini\Controller\CLI;

class User extends \Gini\Controller\CLI {

    function actionGet () {
        $db = \Gini\Database::db('lims');
        $users = $db->query("SELECT * FROM user")->rows();

        if ( count($users) ) {
            foreach ($users as $user) {
                if (!$user->token) continue;
                $token = $user->token . '%billing.swu';
                echo $token."\n";
                $u = a('user', ['username' => $token]);
                if (!$u->id) $u->username = $token;
                $u->update();
            }
        }

        echo "   \e[32mdone.\e[0m\n";
    }

}
