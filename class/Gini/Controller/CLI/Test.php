<?php

namespace Gini\Controller\CLI {

    class Test extends \Gini\Controller\CLI {

        function actionDeposit() {
            $user = a('user', 1);
            $res = m('fund')->get('wa')[2];
            $fund = a('fund')->whose('dept_no')->is($res['BMBH'])
                ->andWhose('prot_no')->is($res['XMBH']);
            
            $fund->dept_name = $res['BMMC'];
            $fund->dept_no = $res['BMBH'];
            $fund->prot_name = $res['XMMC'];
            $fund->prot_no = $res['XMBH'];
            if ($fund->save()) {
                $balance = a('account');
                $balance->fund = $fund;
                $balance->user = $user;
                $balance->lab = $user->lab;
                $balance->balance = 30000;
                $balance->save();
            }
        }

        function actionD () {
            $config = \Gini\Config::get('billing.distribution');
            $equipment = a('equipment', 1);
            echo print_r($equipment->group, true);
            echo print_r($config['manager'], true);
        }

    }
}