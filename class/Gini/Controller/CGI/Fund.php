<?php

namespace Gini\Controller\CGI;

class Fund extends Layout\Dashboard {

    function __index($start = 1) {
        $me = _G('ME');
        $form = $this->form('post');
        
        $filter = [
            'user' => $me->oid,
            'sortby' => ['disable', 'show'],
            'order' => ['asc', 'desc'],
        ];

        if ($form) {
             $filter = array_merge($filter, $form);
        }

        $remote = thoseIndexed('fund\user')->filter($filter);
        $connects = $remote->fetch($start);
        $total = $remote->total();
        
        $pagination = \Gini\Model\Widget::factory('pagination', [
            'uri' => 'fund',
            'total' => $total,
            'start' => $start,
        ]);

        $search = \Gini\Model\Widget::factory('search', [
            'form' => $form,
            'fields' => $this->fields()
        ]);
        
        $this->view->body = V('fund/list', [
            'form' => $form,
            'connects' => $connects,
            'pagination' => $pagination,
            'search' => $search,
        ]);
    }

    protected function fields () {
        return [
            'like_id' => [
                'title' => '编号',
                'type' => 'text'
            ],
            'dept_no' => [
                'title' => '部门编号',
                'placeholder' => '请输入部门编号',
                'type' => 'text'
            ],
            'dept_name' => [
                'title' => '部门名称',
                'type'  => 'text'
            ],
            'prot_no' => [
                'title' => '项目编号',
                'type'  => 'text'
            ],
            'prot_name' => [
                'title' => '项目名称',
                'type'  => 'text'
            ],
            'ctime' => [
                'title' => '操作时间',
                'type'  => 'time'
            ]
        ];
    }

}