<?php

namespace Gini\Controller\CGI;

class Transaction extends Layout\Dashboard {

    function __index($id = 0, $start = 1) {
        $me = _G('ME');
        $form = $this->form('post');

        $filter = [
            'pi' => $me->oid,
            'sortby' => 'ctime',
            'order' => 'desc',
        ];

        if ($id) {
            $filter['account'] = $id;
            $account = a('account', $id);
        }
        else if ($form) {
            $filter = array_merge($filter, $form);
        }

        $remote = thoseIndexed('transaction')->filter($filter);
        $transactions = $remote->fetch($start);
        $total = $remote->total();

        $pagination = \Gini\Model\Widget::factory('pagination', [
            'uri' => "transaction/{$id}",
            'total' => $total,
            'start' => $start,
        ]);

        $search = \Gini\Model\Widget::factory('search', [
            'form' => $form,
            'fields' => $this->fields()
        ]);
        
        $this->view->body = V('transaction/list', [
            'form' => $form,
            'account' => $account,
            'pagination' => $pagination,
            'transactions' => $transactions,
            'search' => $search,
        ]);
    }

    function actionMy($start = 1) {
        $me = _G('ME');
        $form = $this->form('post');
        $this->selected = 'transaction/my';

        $filter = [
            'user' => $me->oid,
            'sortby' => 'ctime',
            'order' => 'desc',
        ];
        
        if ($form) {
            $filter = array_merge($filter, $form);
        }

        $transactions = thoseIndexed('transaction')->filter($filter)->fetch();

        $search = \Gini\Model\Widget::factory('search', [
            'form' => $form,
            'fields' => $this->fields()
        ]);
        
        $this->view->body = V('transaction/list', [
            'form' => $form,
            'transactions' => $transactions,
            'search' => $search
        ]);
    }
    
    /*
        系统财务明细
    */
    public function actionSystem($start = 1) {
        $me = _G('ME');
        if (!$me->isAdmin()) {
            $this->redirect('error/401');
        }
        $form = $this->form('post');
        $this->selected = 'transaction/system';

        $filter = [
            // 'user' => $me->oid,
            'sortby' => 'ctime',
            'order' => 'desc',
        ];
        
        if ($form) {
            $filter = array_merge($filter, $form);
        }

        $remote = thoseIndexed('transaction')->filter($filter);
        $transactions = $remote->fetch($start);
        $total = $remote->total();

        $pagination = \Gini\Model\Widget::factory('pagination', [
            'uri' => 'transaction/system',
            'total' => $total,
            'start' => $start,
        ]);

        $search = \Gini\Model\Widget::factory('search', [
            'form' => $form,
            'fields' => $this->systemfields()
        ]);
        
        $this->view->body = V('transaction/list', [
            'form' => $form,
            'pagination' => $pagination,
            'transactions' => $transactions,
            'search' => $search,
        ]);
    }

     /*
        我的授权财务明细
    */
    public function actionAuthorized($id) {
        $me = _G('ME');
        $form = $this->form('post');
        $this->selected = 'authorized/my';

        $filter = [
            'user' => $me->oid,
            'authorized' => (int)$id,
            'sortby' => 'ctime',
            'order' => 'desc',
        ];
        
        if ($form) {
            $filter = array_merge($filter, $form);
        }

        $remote = thoseIndexed('transaction')->filter($filter);
        $transactions = $remote->fetch($start);
        $total = $remote->total();
        $pagination = \Gini\Model\Widget::factory('pagination', [
            'uri' => 'transaction/my',
            'total' => $total,
            'start' => $start,
        ]);

        $search = \Gini\Model\Widget::factory('search', [
            'form' => $form,
            'fields' => $this->fields()
        ]);
        
        $this->view->body = V('transaction/authorized', [
            'form' => $form,
            'pagination' => $pagination,
            'transactions' => $transactions,
            'search' => $search,
            'aid' => (int)$id
        ]);
    }
    protected function systemfields () {
        return [
            'like_id' => [
                'title' => '明细号',
                'type' => 'text'
            ],
            'lab' => [
                'title' => '课题组',
                'type' => 'text'
            ],
            'dept_no' => [
                'title' => '部门编号',
                'type' => 'text'
            ],
            'dept_name' => [
                'title' => '部门名称',
                'type'  => 'text'
            ],
            'prot_no' => [
                'title' => '项目编号',
                'type'  => 'text'
            ],
            'prot_name' => [
                'title' => '项目名称',
                'type'  => 'text'
            ],
            'type' => [
                'title' => '操作类别',
                'type'  => 'select',
                'values' => \Gini\ORM\Transaction::$AMOUNT_LABEL
            ],
            'user_name' => [
                'title' => '操作人',
                'type'  => 'text'
            ],
            'ctime' => [
                'title' => '操作时间',
                'type'  => 'time'
            ]
        ];
    }

        protected function fields () {
        return [
            'like_id' => [
                'title' => '明细号',
                'type' => 'text'
            ],
            'dept_no' => [
                'title' => '部门编号',
                'type' => 'text'
            ],
            'dept_name' => [
                'title' => '部门名称',
                'type'  => 'text'
            ],
            'prot_no' => [
                'title' => '项目编号',
                'type'  => 'text'
            ],
            'prot_name' => [
                'title' => '项目名称',
                'type'  => 'text'
            ],
            'type' => [
                'title' => '操作类别',
                'type'  => 'select',
                'values' => \Gini\ORM\Transaction::$AMOUNT_LABEL
            ],
            'user_name' => [
                'title' => '操作人',
                'type'  => 'text'
            ],
            'ctime' => [
                'title' => '操作时间',
                'type'  => 'time'
            ]
        ];
    }

}
