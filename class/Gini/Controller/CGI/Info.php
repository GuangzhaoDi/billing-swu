<?php

namespace Gini\Controller\CGI;

class Info extends Layout\Dashboard {

    function __index() {
        $me = _G('ME');
        $form = $this->form('post');
        $accounts = thoseIndexed('account')->filter([
            'lab' => $me->lab->oid
        ])->fetch();

        $filter = [
            'account' => array_column($accounts, 'id') + [0],
            'compare' => [
                'balance', '>', 0
            ],
            'sortby' => 'ctime',
            'order' => 'desc',
        ];
        
        if ($form) {
            $filter['keyword'] = $form['keyword'];
        }
        
        $authorizeds = thoseIndexed('authorized')->filter($filter)->fetch();
        
        $this->view->body = V('info/list', [
            'form' => $form,
            'authorizeds' => $authorizeds,
        ]);
    }

}