<?php

namespace Gini\Controller\CGI;

class Confirm extends Layout\Dashboard {

    function __index() {
        $me = _G('ME');
        $form = $this->form('post');

        $equipments = those('user/equipment')->whose('user')->is($me)->get('equipment');
        $records = thoseIndexed('authorized\record')->filter([
                'operator' => $me->oid,
                'sortby' => 'ctime',
                'order' => 'desc',
            ])->fetch();
        
        $this->view->body = V('confirm/list', [
            'records' => $records
        ]);
    }

}