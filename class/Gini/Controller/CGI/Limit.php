<?php

namespace Gini\Controller\CGI;

class Limit extends Layout\Dashboard {

    function __index() {
        $me = _G('ME');
        $form = $this->form('post');
        $config = a('config', ['key' => 'deposit.limit']);
        $limit = $config->id ? $config->value : \Gini\Config::get('app.billing')['deposit']['limit'];

        if ($form) {
            $validator = new \Gini\CGI\Validator;
            try {
                $validator
                    ->validate('limit', 
                    (int)$form['limit'] >= 0, T('请输入正确值!'));
                    
                $validator->done();

                $config->key = 'deposit.limit';
                $config->value = (int)$form['limit'];
                if ($config->save()) {
                    $logger = \Gini\IoC::construct('\Gini\Logger', 'config');
                    $logger->info('用户[{userId}]{userName} 将金额限制【{key}】改为[{value}]', [
                        'userId' => $me->oid,
                        'userName' => $me->name,
                        'key' => $config->key,
                        'value' => $config->value
                    ]);
                    alert(T('操作成功'));
                }
                else {
                    $logger = \Gini\IoC::construct('\Gini\Logger', 'config');
                    $logger->error('用户[{userId}]{userName} [ERROR]将金额限制【{key}】改为[{value}] 失败', [
                        'userId' => $me->oid,
                        'userName' => $me->name,
                        'key' => $config->key,
                        'value' => $config->value
                    ]);
                    alert(T('操作失败, 请联系管理员'), 'danger');
                }
            }
            catch (\Gini\CGI\Validator\Exception $e) {
                $form['_errors'] = $validator->errors();
            }
        }
        
        $this->view->body = V('limit/edit', [
            'form' => $form,
            'limit' => $limit,
        ]);
    }

}