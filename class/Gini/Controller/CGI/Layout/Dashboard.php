<?php

namespace Gini\Controller\CGI\Layout;

abstract class Dashboard extends \Gini\Controller\CGI\Layout {

    protected $selected = null;

    function __preAction($action, &$params){
        $me = _G('ME');
        if (!$me->id) $this->redirect('error/401');
        $this->view = V('layout/dashboard');
    }

    function __postAction($action, &$params, $response) {
        $me = _G('ME');
        $route = \Gini\CGI::route();
        if ($route) $args = explode('/', $route);
        if (!$route || count($args) == 0) $args = ['index'];

        if ($me->isAllowedTo('充值')) {
            $funds = thoseIndexed('fund')->filter([
                'lab' => $me->lab->oid,
            ])->fetch();

            foreach ($funds as $key => $item) {
                $fund = a('fund', $item['id']);
                $funds[$key]['balance'] = $fund->account()['balance'];
            }
            
            uasort($funds, function ($a, $b) {
                if ($a['balance'] == $b['balance']) {
                    return 0;
                }
                else $a['balance'] < $b['balance'] ? -1 : 1;
            });
        }
        $this->view->header = V('layout/dashboard/header', [
            'me' => $me,
            'funds' => $funds
        ]);
        $this->view->sidebar = V('layout/dashboard/sidebar', [
            'selected' => $this->selected ? : $args[0],
            'items' => $me->sidebarItems(),
        ]);
        $this->view->footer = V('footer');

        $class = [];
        while (count($args) > 0) {
            $class[] = 'layout-' . implode('-', $args);
            array_pop($args);
        }

        return parent::__postAction($action, $params, $response);
    }
}