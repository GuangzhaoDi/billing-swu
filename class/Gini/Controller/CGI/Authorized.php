<?php

namespace Gini\Controller\CGI;

class Authorized extends Layout\Dashboard {


    function __index() {
        $me = _G('ME');
        $form = $this->form('post');
        $this->selected = 'authorized';
        $accounts = thoseIndexed('account')->filter([
            'lab' => $me->lab->oid
        ])->fetch();

        $filter = [
            'account' => array_column($accounts, 'id') + [0],
            'compare' => [
                'balance', '>', 0
            ],
            'sortby' => 'ctime',
            'order' => 'desc',
        ];
        
        if ($form) {
            $filter = array_merge($filter, $form);
        }

        $authorizeds = thoseIndexed('authorized')->filter($filter)->fetch();

        $search = \Gini\Model\Widget::factory('search', [
            'form' => $form,
            'fields' => $this->auth()
        ]);

        $this->view->body = V('info/list', [
            'form' => $form,
            'authorizeds' => $authorizeds,
            'searchForm' => $search
        ]);
    }

    function actionInfo($aid) {
        $me = _G('ME');
        $form = $this->form('post');
        $this->selected = 'authorized';
        $filter = [
            'operator' => $me->oid,
            'sortby' => 'ctime',
            'order' => 'desc',
            'authorized' => $aid
        ];

        if ($form) {
            $filter = array_merge($filter, $form);
        }

        $remote = thoseIndexed('authorized\record')->filter($filter);
        $records = $remote->fetch($start);
        $total = $remote->total();

        $pagination = \Gini\Model\Widget::factory('pagination', [
            'uri' => 'authorized',
            'total' => $total,
            'start' => $start,
        ]);

        $search = \Gini\Model\Widget::factory('search', [
            'form' => $form,
            'fields' => $this->search()
        ]);

        $this->view->body = V('authorized/list', [
            'form' => $form,
            'records' => $records,
            'pagination' => $pagination,
            'searchForm' => $search
        ]);
    }

    function actionMy () {
        $me = _G('ME');
        $form = $this->form('post');
        $this->selected = 'authorized/my';

        $authorizeds = thoseIndexed('authorized')->filter([
            'user' => $me->oid,
            'sortby' => 'balance',
            'order' => 'desc',
        ])->fetch();
        
        $this->view->body = V('authorized/my', [
            'authorizeds' => $authorizeds
        ]);
    }

    function actionMyinfo ($id,  $start = 1) {
        $me = _G('ME');
        $form = $this->form('post');
        $this->selected = 'authorized/my';
        $filter = [
            // 'licensor_id' => null,
            'only_record' => 1,
            'authorized' => $id,
            'sortby' => 'ctime',
            'order' => 'desc',
        ];
        
        if ($form) {
            $filter = array_merge($filter, $form);
        }
        $remote = thoseIndexed('authorized\record')->filter($filter);
        $auth_record = $remote->fetch($start);
        $total = $remote->total();
        // var_dump($auth_record);exit;
        $pagination = \Gini\Model\Widget::factory('pagination', [
            'uri' => "authorized/myinfo/{$id}",
            'total' => $total,
            'start' => $start,
        ]);

        $search = \Gini\Model\Widget::factory('search', [
            'form' => $form,
            'fields' => $this->fields()
        ]);

        $from = $from ? '经费列表' : '充值冻结';

        $this->view->body = V('authorized/myinfo', [
            'form' => $form,
            'from' => $from,
            'auth_record' => $auth_record,
            'pagination' => $pagination,
            // 'searchForm' => $search,
        ]);
    }
    
    protected function search () {
        return [
            'licensor' => [
                'title' => '被授权人',
                'type'  => 'text'
            ],
            'type' => [
                'title' => '操作类别',
                'type'  => 'select',
                'values' => \Gini\ORM\Authorized\Record::$types_text
            ],
            'ctime' => [
                'title' => '操作时间',
                'type'  => 'time'
            ]
        ];
    }
    
    protected function auth () {
        return [
            'like_id' => [
                'title' => '授权号',
                'type' => 'text'
            ],
            'dept_no' => [
                'title' => '部门编号',
                'type' => 'text'
            ],
            'dept_name' => [
                'title' => '部门名称',
                'type'  => 'text'
            ],
            'prot_no' => [
                'title' => '项目编号',
                'type'  => 'text'
            ],
            'prot_name' => [
                'title' => '项目名称',
                'type'  => 'text'
            ]
        ];
    }
    protected function fields () {
        return [
            'like_id' => [
                'title' => '编号',
                'type' => 'text'
            ],
            'dept_no' => [
                'title' => '部门编号',
                'type' => 'text'
            ],
            'dept_name' => [
                'title' => '部门名称',
                'type'  => 'text'
            ],
            'prot_no' => [
                'title' => '项目编号',
                'type'  => 'text'
            ],
            'prot_name' => [
                'title' => '项目名称',
                'type'  => 'text'
            ],
            'ctime' => [
                'title' => '操作时间',
                'type'  => 'time'
            ]
        ];
    }

}