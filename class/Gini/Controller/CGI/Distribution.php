<?php

namespace Gini\Controller\CGI;
use Endroid\QrCode\QrCode;

class Distribution extends Layout\Dashboard {

    function __index () {
        $me = _G('ME');
        $form = $this->form('post');
        $connect = those('user/equipment');

        $filter = [
            'sortby' => 'ctime',
            'order' => 'desc',
        ];
        
        if ($form['equipment_name']) {
            $connect->whose('equipment')->isIn(
                those('equipment')->whose('name')->contains($form['equipment_name'])->get('oid')
            );
        }

        if ($form) {
            $filter = array_merge($filter, $form);
        }
        
        //如果是机主，只能查看已经确认的分配单
        if ($me->isCharger() && !$me->isAdmin()) {
            $filter['status'] = [
                \Gini\ORM\Distribution::STATUS_ASSIGNED,
                \Gini\ORM\Distribution::STATUS_COMPLETE
            ];
        }

        if (!$me->isAdmin()) {
            $connect->whose('user')->is($me);
        }

        $filter['equipment'] = $connect->get('equipment');
        $distributions = thoseIndexed('distribution')->filter($filter)->fetch();

        $search = \Gini\Model\Widget::factory('search', [
            'form' => $form,
            'fields' => $this->fields()
        ]);
        
        $this->view->body = V('distribution/list', [
            'form' => $form,
            'distributions' => $distributions,
            'searchForm' => $search,
        ]);
    }

    protected function fields () {
        return [
            'like_id' => [
                'title' => '编号',
                'placeholder' => '请输入编号',
                'type' => 'text'
            ],
            'equipment_name' => [
                'title' => '仪器名称',
                'type'  => 'text'
            ],
            'status' => [
                'title' => '状态',
                'type'  => 'select',
                'values' => \Gini\ORM\Distribution::$STATUS_LABEL
            ],
            'ctime' => [
                'title' => '创建时间',
                'type'  => 'time'
            ]
        ];
    }

    //机主用户分配结果
    function actionResult () {

        $me = _G('ME');
        if (!$me->ischarger) {
            $this->redirect('error/401');
        }
        $form = $this->form('post');
        $equipments = those('user/equipment')->whose('user')->is($me);

        if ($form) {
            $equipments->andWhose('equipment')->isIn(
                those('equipment')->whose('name')->contains($form['keyword'])
            );
        }
        
        $filter = [
            'status' => \Gini\ORM\Distribution::STATUS_COMPLETE,
            'sortby' => 'ctime',
            'order' => 'desc',
        ];

        if (!$me->isAdmin()) {
            $filter['equipment'] = $equipments->get('equipment');
        }
        
        $distributions = thoseIndexed('distribution')->filter($filter)->fetch();
        
        $this->view->body = V('distribution/list', [
            'form' => $form,
            'distributions' => $distributions,
        ]);
    }

    function actionInfo ($id = 0) {
        $me = _G('ME');
        $distribution = a('distribution', $id);
        if (!$distribution->id) {
            $this->redirect('distribution');
        }
        if (!$me->isAllowedTo('查看', $distribution) && !$me->isAdmin()) {
            $this->redirect('error/401');
        }

        $filter = [
            'distribution' => $distribution->id,
            'sortby' => 'ctime',
            'order' => 'desc',
        ];
        $transactions = thoseIndexed('transaction')->filter($filter)->fetch();
        $equipment = a('equipment', ['oid' => $distribution->equipment]);
        
        $this->view->body = V('distribution/info', [
            'balance' => abs($distribution->balance),
            'equipment' => $equipment,
            'transactions' => $transactions,
            'distribution' => $distribution->manager(),
        ]);
    }

    function actionExport ($id = 0) {
        $me = _G('ME');
        $distribution = a('distribution', $id);
        if (!$distribution->id) {
            $this->redirect('error/404');
        }

        if (!$me->isAllowedTo('审核')) {
            $this->redirect('error/401');
        }

        $filter = [
            'distribution' => $distribution->id,
            'sortby' => 'ctime',
            'order' => 'desc',
        ];
        $transactions = thoseIndexed('transaction')->filter($filter)->fetch();
        $equipment = a('equipment', ['oid' => $distribution->equipment]);

        $excel = new \PHPExcel();
        $excel->setActiveSheetIndex(0);
        $active = $excel->getActiveSheet();
        $active->setTitle(H('分配概览'));

        $active->setCellValue('A1', '单号: ' . str_pad($distribution->id, 8, 0, STR_PAD_LEFT) . ' 预约号: ' . $distribution->reserve)->mergeCells('A1:D1');
        $active->setCellValue('A2', '金额: ' . $distribution->balance)->mergeCells('A2:D2');

        $active->setCellValue('A3', '账号');
        $active->setCellValue('B3', '类型');
        $active->setCellValue('C3', '分配比例');
        $active->setCellValue('D3', '分配金额');

        $active->setCellValue('A4', $distribution->admin);
        $active->setCellValue('B4', T('大仪共享绩效费'));
        $active->setCellValue('C4', ($distribution->admin_scale * 100) . '%');
        $active->setCellValue('D4', $distribution->balance * $distribution->admin_scale);

        $active->setCellValue('A5', $distribution->manager);
        $active->setCellValue('B5', $equipment->group() ? $equipment->group() . '大仪共享成本费' : T('学院大仪共享成本费'));
        $active->setCellValue('C5', ($distribution->manager_scale * 100) . '%');
        $active->setCellValue('D5', $distribution->balance * $distribution->manager_scale);

        $active->setCellValue('A6', $distribution->school);
        $active->setCellValue('B6', T('维修基金及管理费'));
        $active->setCellValue('C6', ($distribution->school_scale * 100) . '%');
        $active->setCellValue('D6', $distribution->balance * $distribution->school_scale);

        $excel->addSheet(new \PHPExcel_Worksheet($objPHPExcel, H('分配明细')), 1);
        $excel->setActiveSheetIndex(1);
        $active = $excel->getActiveSheet();
        
        $active->setCellValue('A1', '明细号');
        $active->setCellValue('B1', '所属单位');
        $active->setCellValue('C1', '项目编号');
        $active->setCellValue('D1', '金额');
        $active->setCellValue('E1', '补贴');
        $active->setCellValue('F1', '测试时间');
        $active->setCellValue('G1', '预约使用人');
        $active->setCellValue('H1', '测试内容');
        $active->setCellValue('I1', '收费确认');
        $active->setCellValue('J1', '经费负责人');
        $active->setCellValue('K1', '经费审批人');

        $accounts = [];
        $funds = [];
        $index = 2;
        foreach ($transactions as $tran) { 
            if ($tran['object'] == 'subsidy') continue;

            if (!$accounts[$tran['account']]) $accounts[$tran['account']] = a('account', $tran['account']);
            $account = $accounts[$tran['account']];

            if (!$funds[$account->fund]) $funds[$account->fund] = a('fund', $account->fund);
            $fund = $funds[$account->fund];
            $user = a('user', $tran['user']);
            $incharge = a('transaction', ['account' => $account, 'source' => 'szht', '_object' => true]);
            $lab = a('lab', $account->lab);

            $content = '';
            if ($lab->owner->id != $user->id) {
                $record = a('authorized/record', ['licensor' => $tran['user'], 
                'type' => \Gini\ORM\Authorized\Record::TYPE_ALLOW, '_object' => true]);
                $op = a('user', $record->operator);
                $content = H("{$op->name}将经费授权给{$user->name} {$record->ctime}");
            }

            $active->setCellValue('A' . $index, H(str_pad($tran['id'], 8, 0, STR_PAD_LEFT)));
            $active->setCellValue('B' . $index, H(explode('-', $distribution->manager)[0] . $equipment->group()));
            $active->setCellValue('C' . $index, H($fund->prot_no));
            $active->setCellValue('D' . $index, H(abs($tran['amount'])));
            $active->setCellValue('E' . $index, H($tran['note']));
            $active->setCellValue('F' . $index, H($tran['ctime']));
            $active->setCellValue('G' . $index, H("{$user->name}-{$user->lab->name}"));
            $active->setCellValue('H' . $index, H($tran['description']));
            $active->setCellValue('I' . $index, H($tran['comment']));
            $active->setCellValue('J' . $index, H("{$lab->owner->name}充值经费到{$lab->name} {$incharge->ctime}"));
            $active->setCellValue('K' . $index, H($content));
            
            $index ++;
        }

        header("Accept-Ranges:bytes");
        header("Content-type: text/xls");
        header('Content-Disposition: attachment; filename=' . time() . '.xls');
        header("Pragma:no-cache");
        header("Expires: 0");

        $writer = \PHPExcel_IOFactory::createWriter($excel, 'Excel5');
        $writer->save('php://output');
        exit;
    }

    function actionQrcode ($id = 0) {
        $me = _G('ME');
        $distribution = a('distribution', $id);
        if (!$distribution->id) {
            $this->redirect('error/404');
        }

        if (!$me->isAllowedTo('审核')) {
            $this->redirect('error/401');
        }

        $data = implode(';', ['YY',
            $distribution->reserve, //预约单号
            $distribution->balance, //金额
        ]);
        
        $qrCode = new QrCode();
        $qrCode
            ->setText($data)
            ->setSize(150)
            ->setPadding(10)
            ->setErrorCorrection('high')
            ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0])
            ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0])
            ->setImageType(QrCode::IMAGE_TYPE_PNG)
            ->render();
    }

    function actionPrint ($id = 0) {
        $me = _G('ME');
        $distribution = a('distribution', $id);
        if (!$distribution->id) {
            $this->redirect('error/404');
        }

        if (!$me->isAllowedTo('审核')) {
            $this->redirect('error/401');
        }

        $filter = [
            'distribution' => $distribution->id,
            'sortby' => 'ctime',
            'order' => 'desc',
        ];
        $transactions = thoseIndexed('transaction')->filter($filter)->fetch();
        $equipment = a('equipment', ['oid' => $distribution->equipment]);

        $this->view = '';

        $pdf = new \TCPDF();
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);

        // set font
        $fontname = \TCPDF_FONTS::addTTFfont(APP_PATH.'/raw/assets/fonts/heiti.ttf', 'TrueTypeUnicode', '', 32);

        $pdf->AddPage();

        $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 2, 'color' => array(0, 0, 0)));
        $pdf->Line(10, 20, 200, 20, ['width'=>0.5]);
        $pdf->Line(10, 62, 200, 62, ['width'=>0.5]);

        $pdf->SetFont($fontname, '', 14);
        $pdf->MultiCell(30, 10, H(T('仪器名称:')), 0, 'R', false, 1, 10, 35);
        $pdf->MultiCell(30, 10, H(T('单号:')), 0, 'R', false, 1, 10, 40);
        $pdf->MultiCell(30, 10, H(T('报销预约号:')), 0, 'R', false, 1, 10, 45);
        $pdf->MultiCell(30, 10, H(T('总金额:')), 0, 'R', false, 1, 10, 50);

        $pdf->SetFont($fontname, 'B', 14);

        $pdf->MultiCell(100, 25, H($equipment->name), 0, 'L', 0, 0, 40, 35);
        $pdf->MultiCell(100, 25, H(str_pad($distribution->id, 8, 0, STR_PAD_LEFT)), 0, 'L', 0, 0, 40, 40);
        $pdf->MultiCell(100, 25, H($distribution->reserve), 0, 'L', 0, 0, 40, 45);
        $pdf->MultiCell(100, 25, H($distribution->balance . '元'), 0, 'L', 0, 0, 40, 50);

        $style = [
            'border' => 0,
            'padding' => 0,
            'fgcolor' => [0,0,0],
            'bgcolor' => false, //array(255,255,255)
            'module_width' => 1, // width of a single module in points
            'module_height' => 1 // height of a single module in points
        ];

        $pdf->SetFont('freemono', 'B', 10);

        $QRCODE = implode(';', ['YY',
            $distribution->reserve, //预约单号
            $distribution->balance, //金额
        ]);

        $pdf->write2DBarcode($QRCODE, 'QRCODE,L', 165, 30, 30, 30, $style, 'N');

        $pdf->SetFont('helvetica', '', 10);
        $style = [
            'position' => '',
            'align' => 'C',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        ];

        $pdf->SetFont($fontname, 'B', 14);
        $pdf->Text(82, 65, H(T('西南大学大仪共享测试费分配单')));
        $pdf->SetFont($fontname, '', 10);
        $pdf->Text(175, 70, date('Y').'年'.date('m').'月'.date('d').'日');

        $html = (string)V('distribution/pdf', [
            'equipment' => a('equipment', ['oid' => $distribution->equipment]),
            'transactions' => $transactions,
            'distribution' => $distribution,
            'chinese_balance'=>self::num2rmb($distribution->balance),
        ]);

        $pdf->SetFont($fontname, '', 14);
        $pdf->writeHTMLCell(30, 30, 12, 75, $html, 0, 1, 0, true, '1', true);

        $pdfName = str_pad($distribution->id, 8, 0, STR_PAD_LEFT).'.pdf';
        $pdf->Output($pdfName, 'I');
        die;
    }

    /**
     * 人民币小写转大写
     *
     * @param string $number 数值
     * @param string $int_unit 币种单位，默认"元"，有的需求可能为"圆"
     * @param bool $is_round 是否对小数进行四舍五入
     * @param bool $is_extra_zero 是否对整数部分以0结尾，小数存在的数字附加0,比如1960.30，
     *             有的系统要求输出"壹仟玖佰陆拾元零叁角"，实际上"壹仟玖佰陆拾元叁角"也是对的
     * @return string
     */

    private function num2rmb($number = 0, $int_unit = '圆', $is_round = TRUE, $is_extra_zero = FALSE)
    {
        // 将数字切分成两段
        $parts = explode('.', $number, 2);
        $int = isset($parts[0]) ? strval($parts[0]) : '0';
        $dec = isset($parts[1]) ? strval($parts[1]) : '';

        // 如果小数点后多于2位，不四舍五入就直接截，否则就处理
        $dec_len = strlen($dec);
        if (isset($parts[1]) && $dec_len > 2)
        {
            $dec = $is_round
                    ? substr(strrchr(strval(round(floatval("0.".$dec), 2)), '.'), 1)
                    : substr($parts[1], 0, 2);
        }

        // 当number为0.001时，小数点后的金额为0元
        if(empty($int) && empty($dec))
        {
            return '零';
        }

        // 定义
        $chs = array('0','壹','贰','叁','肆','伍','陆','柒','捌','玖');
        $uni = array('','拾','佰','仟');
        $dec_uni = array('角', '分');
        $exp = array('', '万');
        $res = '';

        // 整数部分从右向左找
        for($i = strlen($int) - 1, $k = 0; $i >= 0; $k++)
        {
            $str = '';
            // 按照中文读写习惯，每4个字为一段进行转化，i一直在减
            for($j = 0; $j < 4 && $i >= 0; $j++, $i--)
            {
                $u = $int{$i} > 0 ? $uni[$j] : ''; // 非0的数字后面添加单位
                $str = $chs[$int{$i}] . $u . $str;
            }
            //echo $str."|".($k - 2)."<br>";
            $str = rtrim($str, '0');// 去掉末尾的0
            $str = preg_replace("/0+/", "零", $str); // 替换多个连续的0
            if(!isset($exp[$k]))
            {
                $exp[$k] = $exp[$k - 2] . '亿'; // 构建单位
            }
            $u2 = $str != '' ? $exp[$k] : '';
            $res = $str . $u2 . $res;
        }

        // 如果小数部分处理完之后是00，需要处理下
        $dec = rtrim($dec, '0');

        // 小数部分从左向右找
        if(!empty($dec))
        {
            $res .= $int_unit;

            // 是否要在整数部分以0结尾的数字后附加0，有的系统有这要求
            if ($is_extra_zero)
            {
                if (substr($int, -1) === '0')
                {
                    $res.= '零';
                }
            }

            for($i = 0, $cnt = strlen($dec); $i < $cnt; $i++)
            {
                $u = $dec{$i} > 0 ? $dec_uni[$i] : ''; // 非0的数字后面添加单位
                $res .= $chs[$dec{$i}] . $u;
            }
            $res = rtrim($res, '0');// 去掉末尾的0
            $res = preg_replace("/0+/", "零", $res); // 替换多个连续的0
        }
        else
        {
            $res .= $int_unit . '整';
        }
        return $res;
    }

}