<?php

namespace Gini\Controller\CGI;

class Deposit extends Layout\Dashboard {

    function __index($start = 1) {
        $me = _G('ME');
        $form = $this->form('post');
        
        $filter = [
            'lab' => $me->lab->oid,
        ];
        
        if ($form) {
             $filter = array_merge($filter, $form);
        }

        $remote = thoseIndexed('fund')->filter($filter);
        $funds = $remote->fetch($start);
        $total = $remote->total();

        $pagination = \Gini\Model\Widget::factory('pagination', [
            'uri' => 'deposit',
            'total' => $total,
            'start' => $start,
        ]);

        $search = \Gini\Model\Widget::factory('search', [
            'form' => $form,
            'fields' => $this->fields()
        ]);
        
        $this->view->body = V('deposit/overview', [
            'form' => $form,
            'funds' => $funds,
            'pagination' => $pagination,
            'searchForm' => $search,
        ]);
    }

    function actionFund ($id, $from = 0, $start = 1) {
        $me = _G('ME');
        $form = $this->form('post');
        $this->selected = $from ? 'fund' : 'deposit';
        $filter = [
            'lab' => $me->lab->oid,
            'fund' => $id,
            'sortby' => 'balance',
            'order' => 'desc',
        ];
        
        if ($form) {
            $filter = array_merge($filter, $form);
        }

        $remote = thoseIndexed('account')->filter($filter);
        $accounts = $remote->fetch($start);
        $total = $remote->total();

        $pagination = \Gini\Model\Widget::factory('pagination', [
            'uri' => "deposit/fund/{$id}{$from}",
            'total' => $total,
            'start' => $start,
        ]);

        $search = \Gini\Model\Widget::factory('search', [
            'form' => $form,
            'fields' => $this->fields()
        ]);

        $from = $from ? '经费列表' : '充值冻结';

        $this->view->body = V('deposit/list', [
            'form' => $form,
            'from' => $from,
            'accounts' => $accounts,
            'pagination' => $pagination,
            'searchForm' => $search,
        ]);
    }

    protected function fields () {
        return [
            'like_id' => [
                'title' => '编号',
                'type' => 'text'
            ],
            'dept_no' => [
                'title' => '部门编号',
                'type' => 'text'
            ],
            'dept_name' => [
                'title' => '部门名称',
                'type'  => 'text'
            ],
            'prot_no' => [
                'title' => '项目编号',
                'type'  => 'text'
            ],
            'prot_name' => [
                'title' => '项目名称',
                'type'  => 'text'
            ],
            'ctime' => [
                'title' => '操作时间',
                'type'  => 'time'
            ]
        ];
    }

}