<?php

namespace Gini\Controller\CGI\AJAX;

class Account extends \Gini\Controller\CGI {

    public function actionFund () {
        $me = _G('ME');
        $funds = those('account');
        $result = [];
        $form = $this->form('post');

        if ($form) {
            if (isset($form['query'])) {
                $funds->whose('fund')->isIn(
                    those('fund')->whose('dept_name')->contains($form['query'])
                        ->orWhose('dept_no')->contains($form['query'])
                        ->orWhose('prot_name')->contains($form['query'])
                        ->orWhose('prot_no')->contains($form['query'])
                );
            }
        }

        foreach ($funds as $fund) {
            $balance = '余额:' . $fund->balance;
            $result[] = [
                'id' => $fund->id,
                'name' => "{$balance} [{$fund->fund->dept_no}]{$fund->fund->dept_name} - [{$fund->fund->prot_no}]{$fund->fund->prot_name}",
            ];
        }

        return \Gini\IoC::construct('\Gini\CGI\Response\JSON', $result);
    }

}