<?php

namespace Gini\Controller\CGI\AJAX;

class Deposit extends \Gini\Controller\CGI {

    public function actionAdd () {
        $me = _G('ME');
        $form = $this->form('post');
        $config = a('config', ['key' => 'deposit.limit']);
        $limit = $config->id ? $config->value : \Gini\Config::get('app.billing')['deposit']['limit'];

        $filter = [
            'user' => $me->oid,
            'disable' => 0,
            'sortby' => 'show',
            'order' => 'desc',
        ];
        $remote = thoseIndexed('fund\user')->filter($filter);
        $funds = $remote->fetch(0);
        $labs = those('lab');

        if ($form) {
            $validator = new \Gini\CGI\Validator;
            try {
                $validator
                    ->validate('fund', !!$form['fund'], T('经费不能为空!'))
                    ->validate('lab', !!$form['lab'], T('课题组不能为空!'))
                    ->validate('balance', !!$form['balance'], T('金额不能为空!'))
                    ->validate('balance', $form['balance'] > 0, T('金额必须为正数!'));
                
                if ($form['fund']) {
                    $fund = a('fund', $form['fund']);
                    $balance = $fund->balance();
                    if ($balance < (int)$form['balance']) $validator->validate('balance', false, T('经费余额不足!'));
                }

                $accounts = thoseIndexed('account')->filter(['lab' => $form['lab']])->fetch();
                $balance = array_sum(array_column($accounts, 'total'));
                if ((int)$limit <= (int)$balance + (int)$form['balance']) $validator->validate('balance', false, T("账户内余额不得超过{$limit}!"));

                $validator->done();

                $account = a('account');
                    
                $account->fund = $fund->id;
                $account->lab = $form['lab'];
                $account->balance = 0;
                $account->total = 0;
                if (!$account->save()) {
                    alert(T('操作失败, 请联系管理员'), 'danger');
                    goto output;
                }

                $transaction = a('transaction');
                $transaction->account = $account->id;
                $transaction->authorized = null;
                $transaction->user = $me->oid;
                $transaction->amount = (int)$form['balance'];
                $transaction->transfer = true;
                $transaction->source = 'szht';

                if ($transaction->save()) {
                    $logger = \Gini\IoC::construct('\Gini\Logger', 'deposit');
                    $logger->info('用户[{userId}]{userName} 账户[{accountId}]充值给课题组{accountLab}金额{balance}', [
                        'userId' => $me->oid,
                        'userName' => $me->name,
                        'accountId' => $transaction->account,
                        'accountLab' => $account->lab,
                        'balance' => $transaction->amount,
                    ]);
                    alert(T('操作成功'));
                }
                else {
                    $logger = \Gini\IoC::construct('\Gini\Logger', 'deposit');
                    $logger->error('用户[{userId}]{userName} [ERROR]账户[{accountId}]充值给课题组{accountLab}金额{balance} 失败', [
                        'userId' => $me->oid,
                        'userName' => $me->name,
                        'accountId' => $transaction->account,
                        'accountLab' => $account->lab,
                        'balance' => $transaction->amount,
                    ]);
                    alert(T('操作失败, 请联系管理员'), 'danger');
                }

                output:
                return \Gini\IoC::construct('\Gini\CGI\Response\Redirect', 'deposit');
            }
            catch (\Gini\CGI\Validator\Exception $e) {
                $form['_errors'] = $validator->errors();
            }
        }

        $view = V('deposit/add', [
            'labs' => $labs,
            'form' => $form,
            'limit' => $limit,
            'funds' => $funds,
        ]);

        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', $view);
    }

    public function actionBatchUnfreeze($fund_id = null) {

        $me = _G('ME');
        $form = $this->form('post');

                error_log(print_r(123456, true));
        //确认解冻
        if ($form) {
            try {
                if (!is_numeric($form['fund_id'])) {
                    alert(T('操作失败, 请联系管理员'), 'danger');
                    goto redirect;
                }
                if (!is_numeric($form['money']) || $form['money'] <= 0) {
                    alert(T('请输入合法解冻金额'), 'danger');
                    goto redirect;
                }
                $filter['fund_id'] = $form['fund_id'];
                $filter['lab_id'] = $me->lab_id;
                $accounts = thoseIndexed('account')->filter([
                    'lab' => $me->lab->oid,
                    'fund' => $form['fund_id']
                ])->fetch();
                $max_total = 0;
                foreach ($accounts as $a_value) {
                    $max_total += $a_value['balance'];
                }
                if ($max_total < $form['money']) {
                    alert(T('请输入合法解冻金额'), 'danger');
                    goto redirect;
                }
                $filter['money'] = $form['money'];
                $fund = a('fund');
                $response = $fund->unfreeze($filter);
                if ($response) {
                    $logger = \Gini\IoC::construct('\Gini\Logger', 'Deposit');
                    $logger->info('用户[{userId}]{userName} 将授权账户[{authorizedId}]内的用户[{licensorId}]{licensorName}的授权取消', [
                        'userId' => $me->oid,
                        'userName' => $me->name,
                        'licensorId' => $user->oid,
                        'licensorName' => $user->name,
                        'authorizedId' => $authorized->id,
                    ]);
                    alert(T('操作成功'));
                }
                else {
                    alert(T('操作失败, 请联系管理员'), 'danger');
                }
            }
            catch (\Gini\CGI\Validator\Exception $e) {
                alert(T('操作失败, 请联系管理员'), 'danger');
            }

            redirect:
            return \Gini\IoC::construct('\Gini\CGI\Response\Redirect', 'deposit');
        }

        $filter = [
            'lab' => $me->lab->oid,
            'fund' => $fund_id
        ];


        $remote = thoseIndexed('account')->filter($filter);
        $funds = $remote->fetch(0);
        $view = V('deposit/batch_unfreeze', [
            'funds' => $funds,
        ]);
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', $view);
    }

    public function actionUnfreeze ($account_id = null) {
        $me = _G('ME');
        $form = $this->form('post');
        $account = a('account', $account_id);
        //确认解冻
        if ($form) {
            $filter['account_id'] = $form['account_id'];
            $filter['lab_id'] = $me->lab_id;
            $fund = a('fund');
            $response = $fund->unfreeze($filter);
            try {
                if (!is_numeric($form['account_id'])) {
                    alert(T('操作失败, 请联系管理员'), 'danger');
                    goto redirect;
                }
                if (!is_numeric($form['money']) || $form['money'] <= 0) {
                    alert(T('请传入合法金额'), 'danger');
                    goto redirect;
                }
                $filter['lab_id'] = $me->lab_id;
                $filter['account_id'] = $form['account_id'];
                $filter['money'] = $form['money'];
                $account = a('account', $filter['account_id']);
                
                if (!$account->id || $account->lab != $filter['lab_id']) {
                   alert(T('没有权限, 请联系管理员'), 'danger');
                    goto redirect; 
                }

                $fund = a('fund');
                $response = $fund->unfreeze($filter);

                if ($response) {
                    $logger = \Gini\IoC::construct('\Gini\Logger', 'Deposit');
                    $logger->info('用户[{userId}]{userName} 将授权账户[{authorizedId}]内的用户[{licensorId}]{licensorName}的授权取消', [
                        'userId' => $me->oid,
                        'userName' => $me->name,
                        'licensorId' => $user->oid,
                        'licensorName' => $user->name,
                        'authorizedId' => $authorized->id,
                    ]);
                    alert(T('操作成功'));
                }
                else {
                    alert(T('操作失败, 请联系管理员'), 'danger');
                }
            }
            catch (\Gini\CGI\Validator\Exception $e) {
                alert(T('操作失败, 请联系管理员'), 'danger');
            }

            redirect:
            return \Gini\IoC::construct('\Gini\CGI\Response\Redirect', "deposit/fund/{$account->fund}");
        }

        if (!is_numeric($account_id)) {
            alert(T('参数错误'), 'danger');
        }

        $view = V('deposit/unfreeze', [
            'account' => $account
        ]);
        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', $view);
    }
       
}