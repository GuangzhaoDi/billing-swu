<?php

namespace Gini\Controller\CGI\AJAX;

class Authorized extends \Gini\Controller\CGI {

    public function actionAdd () {
        $me = _G('ME');
        $form = $this->form('post');
        $accounts = thoseIndexed('account')->filter([
            'lab' => $me->lab->oid,
        ])->fetch();
        
        if ($form) {
            $validator = new \Gini\CGI\Validator;
            try {
                $validator
                    ->validate('account', !!$form['account'], T('经费不能为空!'))
                    ->validate('user', !!$form['user'], T('用户不能为空!'))
                    ->validate('credit', !!$form['credit'], T('授权金额不能为空!'))
                    ->validate('credit', $form['credit'] > 0, T('授权金额不能为负!'));
                //暂时无法灵活限制前端可选择经费数量 这里就只取第一个
                $account = a('account', $form['account']);
                
                if ($account->balance < (int)$form['credit']) {
                    $validator->validate('credit', false, T('没有足够的可授权经费余额!'));
                }

                $validator->done();
                
                $authorized = a('authorized');
                $authorized->account = $form['account'];
                $authorized->balance = 0;
                if ($authorized->save() && $authorized->update(['diff' => $form['credit'], 'me' => $me->oid])) {
                    $users = $form['user'];
                    foreach ($users as $id => $name) {
                        if ($authorized->connect($id)) {
                            $logger = \Gini\IoC::construct('\Gini\Logger', 'authorized');
                            $logger->info('用户[{userId}]{userName} 将[{accountId}]账户内[{price}]授权给用户[{licensorId}]{licensorName} 生成授权账户[{authorizedId}]', [
                                'userId' => $me->oid,
                                'userName' => $me->name,
                                'accountId' => $account->id,
                                'price' => $form['credit'],
                                'licensorId' => $id,
                                'licensorName' => $name,
                                'authorizedId' => $authorized->id,
                            ]);
                        }
                    }
                    alert(T('操作成功'));
                }
                else {
                    alert(T('操作失败, 请联系管理员'), 'danger');
                }

                return \Gini\IoC::construct('\Gini\CGI\Response\Redirect', 'authorized');
            }
            catch (\Gini\CGI\Validator\Exception $e) {
                $form['_errors'] = $validator->errors();
            }
        }

        $view = V('authorized/add', [
            'form' => $form,
            'accounts' => $accounts,
        ]);

        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', $view);
    }

    public function actionDelete ($id = 0, $userId = 0) {
        
        $me = _G('ME');
        $form = $this->form('post');
        $authorized = a('authorized', $id);
        $user = a('user', ['oid' => $userId]);

        if ($form) {
            try {
                $authorized = a('authorized', $form['authorized']);
                $user = a('user', ['oid' => $form['user']]);
                if (!$user->id || !$authorized->id) {
                    alert(T('操作失败, 请联系管理员'), 'danger');
                    goto redirect;
                }
                
                if ($authorized->disconnect($user->oid)) {
                    $logger = \Gini\IoC::construct('\Gini\Logger', 'authorized');
                    $logger->info('用户[{userId}]{userName} 将授权账户[{authorizedId}]内的用户[{licensorId}]{licensorName}的授权取消', [
                        'userId' => $me->oid,
                        'userName' => $me->name,
                        'licensorId' => $user->oid,
                        'licensorName' => $user->name,
                        'authorizedId' => $authorized->id,
                    ]);
                    alert(T('操作成功'));
                }
                else {
                    alert(T('操作失败, 请联系管理员'), 'danger');
                }
            }
            catch (\Gini\CGI\Validator\Exception $e) {
                alert(T('操作失败, 请联系管理员'), 'danger');
            }

            redirect:
            return \Gini\IoC::construct('\Gini\CGI\Response\Redirect', 'info');
        }

        $view = V('authorized/delete', [
            'user' => $user,
            'authorized' => $authorized
        ]);

        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', $view);
    }

    public function actionDeduct ($id) {
        $me = _G('ME');
        $form = $this->form('post');
        $authorized = a('authorized', $id);

        if (!$authorized->id) {
            alert(T('操作失败, 请联系管理员'), 'danger');
            return \Gini\IoC::construct('\Gini\CGI\Response\Redirect', 'info');
        }

        if ($form) {
            $validator = new \Gini\CGI\Validator;
            try {
                $validator
                    ->validate('credit', !!$form['credit'], T('削减金额不能为空!'));
                
                if ($authorized->balance < (int)$form['credit']) {
                    $validator->validate('credit', false, T('没有足够的授权余额可供削减!'));
                }
                
                $validator->done();
                
                $record = a('authorized/record');
                $record->authorized = $authorized->id;
                $record->operator = $me->oid;
                $record->balance = $form['credit'];
                $record->type = \Gini\ORM\Authorized\Record::TYPE_DEDUCT;
                if ($record->save()) {
                    $logger = \Gini\IoC::construct('\Gini\Logger', 'authorized');
                    $logger->info('用户[{userId}]{userName} 将[{authorizedId}]授权账户内可用授权金额削减[{price}]', [
                        'userId' => $me->oid,
                        'userName' => $me->name,
                        'accountId' => $account->id,
                        'price' => $form['credit'],
                        'authorizedId' => $authorized->id,
                    ]);
                    alert(T('操作成功'));
                }
                else {
                    alert(T('操作失败, 请联系管理员'), 'danger');
                }

                return \Gini\IoC::construct('\Gini\CGI\Response\Redirect', 'info');
            }
            catch (\Gini\CGI\Validator\Exception $e) {
                $form['_errors'] = $validator->errors();
            }
        }

        $view = V('authorized/deduct', [
            'form' => $form,
            'authorized' => $authorized,
        ]);

        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', $view);
    }

    public function actionPut ($id) {
        $me = _G('ME');
        $form = $this->form('post');
        $authorized = a('authorized', $id);
        $account = a('account', $authorized->account);
        
        if ($form) {
            $validator = new \Gini\CGI\Validator;
            try {
                $validator
                    ->validate('credit', !!$form['credit'], T('授权金额不能为空!'))
                    ->validate('credit', $form['credit'] > 0, T('授权金额不能为负!'));
                
                if ($account->balance < (int)$form['credit']) {
                    $validator->validate('account', false, T('没有足够的可授权经费余额!'));
                }

                $validator->done();
                
                if ($authorized->update([
                        'diff' => $form['credit'],
                        'me' => $me->oid,
                    ])) {
                    $logger = \Gini\IoC::construct('\Gini\Logger', 'authorized');
                    $logger->info('用户[{userId}]{userName} 将[{authorizedId}]授权账户内可用授权金额添加[{price}]', [
                        'userId' => $me->oid,
                        'userName' => $me->name,
                        'accountId' => $account->id,
                        'price' => $form['credit'],
                        'authorizedId' => $authorized->id,
                    ]);
                    alert(T('操作成功'));
                }
                else {
                    $logger = \Gini\IoC::construct('\Gini\Logger', 'authorized');
                    $logger->error('用户[{userId}]{userName} [ERROR]将[{authorizedId}]授权账户内可用授权金额添加[{price}] 失败', [
                        'userId' => $me->oid,
                        'userName' => $me->name,
                        'accountId' => $id,
                        'price' => $form['credit']
                    ]);
                    alert(T('操作失败, 请联系管理员'), 'danger');
                }

                return \Gini\IoC::construct('\Gini\CGI\Response\Redirect', 'info');
            }
            catch (\Gini\CGI\Validator\Exception $e) {
                $form['_errors'] = $validator->errors();
            }
        }

        $view = V('authorized/put', [
            'form' => $form,
            'account' => $account,
            'authorized' => $authorized,
        ]);

        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', $view);
    }

    public function actionJump ($id) {
        //方法名为什么是jump呢 因为是在已授权的auth下增加用户 就是插队的意思
        $me = _G('ME');
        $form = $this->form('post');
        $authorized = a('authorized', $id);
        
        if ($form) {
            $validator = new \Gini\CGI\Validator;
            try {
                $validator
                    ->validate('authorized', !!$form['authorized'], T('经费不能为空!'))
                    ->validate('user', !!$form['user'], T('用户不能为空!'));

                $validator->done();
                
                $authorized = a('authorized', $form['authorized']);
                $account = a('account', $authorized->account);
                if ($authorized->id) {
                    $users = json_decode($form['user'], true);
                    foreach ($users as $id => $name) {
                        if ($authorized->connect($id)) {
                            $logger = \Gini\IoC::construct('\Gini\Logger', 'authorized');
                            $logger->info('用户[{userId}]{userName} 将[{accountId}]账户内[{price}]授权给用户[{licensorId}]{licensorName} 加入授权账户[{authorizedId}]', [
                                'userId' => $me->oid,
                                'userName' => $me->name,
                                'accountId' => $account->id,
                                'price' => $authorized->balance,
                                'licensorId' => $user->oid,
                                'licensorName' => $user->name,
                                'authorizedId' => $authorized->id,
                            ]);
                        } else {
                            $logger = \Gini\IoC::construct('\Gini\Logger', 'authorized');
                            $logger->error('用户[{userId}]{userName} [ERROR]将[{accountId}]账户内[{price}]授权给用户[{licensorId}]{licensorName} 加入授权账户[{authorizedId}] 失败', [
                                'userId' => $me->oid,
                                'userName' => $me->name,
                                'accountId' => $account->id,
                                'price' => $authorized->balance,
                                'licensorId' => $user->oid,
                                'licensorName' => $user->name,
                                'authorizedId' => $authorized->id,
                            ]);
                        }
                    }
                    alert(T('操作成功'));
                }
                else {
                    alert(T('操作失败, 请联系管理员'), 'danger');
                }

                return \Gini\IoC::construct('\Gini\CGI\Response\Redirect', 'authorize');
            }
            catch (\Gini\CGI\Validator\Exception $e) {
                $form['_errors'] = $validator->errors();
            }
        }

        $view = V('authorized/jump', [
            'authorized' => $authorized
        ]);

        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', $view);
    }

    public function actionUser ($id) {
        $me = _G('ME');
        
        $connects = thoseIndexed('authorized\user')->filter([
                'authorized' => $id
            ])->fetch();
        $result = [];
        
        foreach ($connects as $connect) {
            $user = a('user', ['oid' => $connect['user']]);
            $result[] = [
                'id' => $connect['id'],
                'user' => $user->oid,
                'name' => $user->name,
                'lab' => $user->lab->name,
            ];
        }

        return \Gini\IoC::construct('\Gini\CGI\Response\JSON', $result);
    }

}
