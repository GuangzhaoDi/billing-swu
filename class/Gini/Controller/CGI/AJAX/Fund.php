<?php

namespace Gini\Controller\CGI\AJAX;

class Fund extends \Gini\Controller\CGI {

    public function actionAdd ($id = 0) {
        $me = _G('ME');
        $form = $this->form('post');

        if ($form['type'] == 'show' && $form['value'] == 1) {
            $filter = [
                'user' => $me->oid,
                'show' => 1,
            ];

            $remote = thoseIndexed('fund\user')->filter($filter);
            $connects = $remote->fetch();
            $total = $remote->total();
            
            if ($total >= 5) {
                return \Gini\IoC::construct('\Gini\CGI\Response\JSON', false);
            }
        }

        $connect = a('fund/user', $id);
        
        if ($connect->id && $form) {
            switch ($form['type']) {
                case 'disable':
                    $connect->disable = $form['value'] ? 0 : 1;
                    break;
                case 'show':
                    $connect->show = $form['value'];
                    break;
            }
            if ($connect->save()) {
                $logger = \Gini\IoC::construct('\Gini\Logger', 'fund');
                $logger->info('用户[{userId}]{userName} FUND/USER [{fundUserId}] disable[{disable}] show[{show}]', [
                    'userId' => $me->oid,
                    'userName' => $me->name,
                    'fundUserId' => $connect->id,
                    'disable' => $connect->disable,
                    'show' => $connect->show,
                ]);
            }
        }
        
        return \Gini\IoC::construct('\Gini\CGI\Response\JSON', !!$connect->{$form['type']});
        
    }

    public function actionBalance ($id = 0) {
        $me = _G('ME');
        $fund = a('fund', $id);
        
        return \Gini\IoC::construct('\Gini\CGI\Response\JSON', (int)$fund->balance());
    }

    public function actionRefresh () {
        $me = _G('ME');
        $funds = thoseIndexed('fund')->filter(['ref' => $me->ref])->fetch();

        alert('更新成功');
        return \Gini\IoC::construct('\Gini\CGI\Response\Redirect', 'fund');
    }

}