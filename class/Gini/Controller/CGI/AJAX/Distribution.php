<?php

namespace Gini\Controller\CGI\AJAX;

class Distribution extends \Gini\Controller\CGI {

    public function actionConfirm ($id = 0) {
        $me = _G('ME');
        $form = $this->form('post');
        $distribution = a('distribution', $id);

        if (!$id || !$me->isAllowedTo('审核') && !$me->isAdmin()) {
            return \Gini\IoC::construct('\Gini\CGI\Response\Redirect', '401');
        }

        if ($form) {
            $distribution = a('distribution', $form['id']);
            if ($distribution->status == \Gini\ORM\Distribution::STATUS_PREPARE) {
                $distribution->status = \Gini\ORM\Distribution::STATUS_COMPLETE;
                $distribution->reviewe = $me->oid;
            }
            
            if ($distribution->save()) {
                $logger = \Gini\IoC::construct('\Gini\Logger', 'billing');
                $logger->info('用户[{userId}]{userName} 将[{distributionId}]分配单的状态确认为 {status}', [
                    'userId' => $me->oid,
                    'userName' => $me->name,
                    'distributionId' => $distribution->id,
                    'status' => \Gini\ORM\Distribution::$STATUS_LABEL[$distribution->status],
                ]);
                alert(T('操作成功'));
            }
            else {
                $logger = \Gini\IoC::construct('\Gini\Logger', 'billing');
                $logger->error('用户[{userId}]{userName} [ERROR]将[{distributionId}]分配单的状态确认为 {status} 失败', [
                    'userId' => $me->oid,
                    'userName' => $me->name,
                    'distributionId' => $distribution->id,
                    'status' => \Gini\ORM\Distribution::$STATUS_LABEL[$distribution->status],
                ]);
                alert(T('操作失败, 请联系管理员'), 'danger');
            }

            return \Gini\IoC::construct('\Gini\CGI\Response\Redirect', 'distribution');
        }

        $view = V('distribution/confirm', [
            'distribution' => $distribution,
        ]);

        return \Gini\IoC::construct('\Gini\CGI\Response\HTML', $view);
    }

}