<?php

namespace Gini\Controller\CGI\AJAX;

class User extends \Gini\Controller\CGI {

    public function actionSearch () {
        $me = _G('ME');
        $users = those('user')->whose('lab')->is($me->lab);
        $result = [];
        $form = $this->form('post');

        if ($form) {
            if (isset($form['query'])) {
                $users->whose('name')->contains($form['query'])
                    ->andWhose('id')->isNot($me->id);
            }
        }

        foreach ($users as $user) {
            $result[] = [
                'id' => $user->oid,
                'name' => $user->name,
                'lab' => $user->lab->name,
            ];
        }

        return \Gini\IoC::construct('\Gini\CGI\Response\JSON', $result);
    }

}