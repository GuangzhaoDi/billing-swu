<?php

namespace Gini\Controller\CGI;

class Index extends Layout\Dashboard {

    function __index() {
        $me = _G('ME');
        if ($me->id) $this->redirect($me->defaultUrl());
    }

    function actionLogout () {
        $username = $this->username;
        list($username, $key) = \Gini\Auth::parseUserName($username);
        
        if ($key) {
            if (!preg_match('/(.+)%([^%]+)$/', $key, $parts)) {
                $url = '';
                goto redirect;
            }
            $backend = \Gini\Auth::backends()[$key];
            $id = $backend['info_server'] ? : $parts[2];
            if (!$id) {
                $url = '';
                goto redirect;
            }
        }
        else {
            $id = array_keys(\Gini\Config::get('server.remote'))[0];
        }
        
        $server = \Gini\Module\BillingSwu::remoteServer($id);
        $client = \Gini\Config::get('rpc.clients')['people'];
        $url = trim($server['api'], 'api');

        redirect:
        \Gini\Auth::logout();
        $this->redirect($url);
    }

}